using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointManager : MonoBehaviour
{
    public static SpawnPointManager instance;

    public GameObject[] spawnPointArray;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    public Vector3 getSpawnPoint()
    {
        return spawnPointArray[Random.Range(0, spawnPointArray.Length)].transform.position;
    }
}
